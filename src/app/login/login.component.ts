import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HardcodedAuthentificationService } from '../service/hardcoded-authentification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = 'anas'
  password='x'
  errorMessage='invalid Credentials'
  invalidLogin = false


  constructor(private router: Router, private hardAuthService: HardcodedAuthentificationService) { }

  ngOnInit() {
  }

  handleLogin(){
    //console.log(this.username);
    if(this.hardAuthService.authenticate(this.username, this.password))
    {
      this.router.navigate(['welcome',this.username])
      this.invalidLogin = false
    }else
    {
      this.invalidLogin = true
    }
  }

}
