import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from "@angular/router";
import { HardcodedAuthentificationService } from "./hardcoded-authentification.service";

@Injectable({
  providedIn: "root",
})
export class RouteGuardService implements CanActivate {
  constructor(private hardAuthService: HardcodedAuthentificationService,
    private router:Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.hardAuthService.isUserLoggedIn())
    {
      return true;
    }
    this.router.navigate(['login']);
    return false;
  }
}
